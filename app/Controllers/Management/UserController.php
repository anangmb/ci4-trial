<?php

namespace App\Controllers\Management;

use App\Controllers\BaseController;
use App\Models\User;

class UserController extends BaseController
{
    protected $model;
    
    public function __construct()
    {
        $this->model = new User();
    }

    public function index()
    {
        $users = $this->model
            ->orderBy('user.name','asc')
            ->join('role','user.role_id = role.id')
            ->findAll();
        return view('admin/user/index', ["users"=>$users]);
    }

    public function create(){
        // TO DISPLAY CREATE USER VIEW
        return view('admin/user/create');

    }

    public function store(){
        // TO ACTUALLY STORE THE DATA TO DATABASE
        // TODO: Validate Input
        $data= [
            "name"=>$this->request->getPost('name'),
            "email"=>$this->request->getPost('email'),
            "password"=>password_hash($this->request->getPost('password'), PASSWORD_BCRYPT),
            "role_id"=>$this->request->getPost('role'),
            "approved"=>TRUE
        ];

        $this->model->insert($data);
        return redirect()->route('user.index');
    }

    public function detail(){
        // TO DISLAY USER DETAIL
    }

    public function edit(){
        // TO DISPLAY EDIT USER VIEW
    }

    public function update(){
        // TO ACTUALLY UPDATE THE DATA TO DATABASE
    }

    public function delete(){
        // TO DELETE DATA FROM DATABASE
    }
}
