<?php $this->extend('templates/admin/admin_layout')?>

<?php $this->section('content')?>
<div class="container mt-4">
    <h2 class="d-inline align-middle me-4 ">User Management!</h2>
    <a class="d-inline align-middle" href="<?php echo route_to('user.create') ?>">
    <button class="btn btn-outline-primary">Tambah Pengguna</button>
</a>

    <div class="row mt-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table class="table table-responsive">
                        <thead>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                        </thead>
                        <tbody>
                            <?php foreach($users as $user): ?>
                                <tr>
                                    <td><?= $user["name"] ?></td>
                                    <td><?= $user["email"] ?></td>
                                    <td><?= $user["role_name"] ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endSection()?>