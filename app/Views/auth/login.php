<div class="card mt-5 shadow" style="max-width: 500px; margin: 0 auto;">
    <div class="card-header bg-primary text-white text-center">
        Login
    </div>
    <div class="card-body text-center">
        <img class="img-thumbnail border-0 img-200" src="/img/Wikimedia-logo.png" style="margin: 5vh auto"/>
        <form method="POST" action="<?php echo route_to('do.login') ?>">

            <?php if(isset($_SESSION['error'])) : ?>
                <small class="text-danger"><?php echo($_SESSION['error']) ?></small>
            <?php endif ?>

            <input class="form form-control" placeholder="E-Mail" name="email" type="email">
            <input class="form mt-3 form-control" placeholder="password"  name="password" type="password">
            <button class="btn mt-4 btn-primary w-100">Login</button>
        </form>
    </div>
</div>