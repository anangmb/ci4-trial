<?php

namespace App\Database\Seeds\Production;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $data =[
            'name' => 'Fira',
            'role_id'=> 1,
            'email'=>'fira@mail.com',
            'password'=>password_hash('password', PASSWORD_BCRYPT),
            'approved'=>TRUE
        ];

        $this->db->table('user')->insert($data);
    }
}
