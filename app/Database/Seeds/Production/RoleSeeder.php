<?php

namespace App\Database\Seeds\Production;

use CodeIgniter\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $datas =[ 
            ['role_name' => 'super_admin'],
            ['role_name' => 'admin'],
            ['role_name' => 'kepala_sekolah'],
        ];

        foreach ($datas as $key => $value) {
            $this->db->table('role')->insert($value);
        }

    }
}
