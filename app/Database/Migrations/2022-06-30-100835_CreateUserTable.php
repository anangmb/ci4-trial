<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUserTable extends Migration
{
    public function up()
    {
        // mendefinisikan properti
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'role_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'  => true
            ],
            'email' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'password' => [
                'type'       => 'TEXT',
            ],
            'approved' => [
                'type' => 'BOOLEAN'
            ]
        ]);

        // mendifinisikan key
        $this->forge->addKey('id', true);

        // mendefinisikan FK
        $this->forge->addForeignKey('role_id', 'role', 'id');

        // mendefinisikan nama table
        $this->forge->createTable('user');
    }

    public function down()
    {
        $this->forge->dropTable('user', true);
    }
}
